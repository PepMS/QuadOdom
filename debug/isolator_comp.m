close all
clear 
clc

load flow_rigid2.mat
load flow_isolated2.mat

% flow_isolated = flow_isolated_rotated;

fig = 1;
fontsize = 30;

y1 = flow_rigid;
y2 = flow_isolated;
xl = min(length(y1),length(y2));
x = 1:xl;


c1 = 'r';
c2 = 'b';

xaxislab = 'tstep';
yaxislab1 = 'pix/s';
yaxislab2 = 'pix/s';

lab1 = 'rigid';
lab2 = 'isolated';

name = 'Isolator Comparison';

hfig = figure(fig);
hold on

h11=subplot(2,1,1);hold on; plot(x,y1(1,1:xl),c1); plot(x,y2(1,1:xl),c2); ylabel(yaxislab1);
h12=subplot(2,1,2);hold on; plot(x,y1(2,1:xl),c1); plot(x,y2(2,1:xl),c2); ylabel(yaxislab2);
xlabel(xaxislab);    
linkaxes([h11 h12],'x');
h=legend([h11],lab1,lab2);

set(h,'interpreter','latex','fontsize',fontsize);

set_figure_params(hfig,name,fontsize,[100 100 1050 640]);

hold off