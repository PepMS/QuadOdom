% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   PX4Flow Initial Simulation Values
% 
%   Initializes all variables needed by the filter for the SIMULATED PX4Flow sensor.
%
%   [params] = px4flow_ini_simu()
%
%   Ouputs:
%       - params: Sensor parameters
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [params] = px4flow_ini_simu()

% Parameters
params.bounds.lower = 0.3; % Sensor minimum range (m)  
params.bounds.upper = 4.0; % Sensor maximum range (m)
params.std.insidebouds = [0.05;25.0;25.0;0.15;0.15];  % Std dev when inside bounds
params.std.outsidebouds = [10;100;100;10;10]; % Std dev when outside bounds
params.datatype = 'flow2d'; % Which data type is used. Currently supported: 'vxy' or 'flow2d'
params.focal = 500; % Focal length in pixels

return