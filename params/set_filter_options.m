% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   FILTER OPTIONS
% 
%   This script sets the structure with all filter types to be executed. 
%   The filter options are used for comparison purposes. 
% 
%   This filter types include:
%       - met = ['eskf','ekf']: Extended Kalman Filter, Error State Kalman 
%                               Filter.
%       - frame = ['global','local']: Orientation Error representation.
%       - qint_met = ['zerof','zerob','first']: Quaternion integration
%                    method.
%       - imu_met = [1,2,3]: Truncation grade of IMU transition matrix.
% 
%   Outputs:
%       - exp:  Structure with the different filter versions to be executed
% 
%   NOTE: The data must be stored in the folder DATA. There must exist two
%   paths: SIMU and REAL (lowercase) for correct execution and set the
%   correct values to the filter depending on the nature of the data. All
%   ROSBAG files are considered REAL.
% 
%   Results will be stored in RESULTS folder. If using data in .MAT files
%   the resulting folder will have the same name as the original data. If
%   using ROSBAG files, a new folder with the same name as the ROSBAG will
%   be created.
% 
%   ROSBAG files can coexist in the same original folder. Just specify the
%   different names.
% 
%   .MAT files are expected to be organized in different folders for each
%   experiments (set of .MAT files).
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [data,exp] = set_filter_options()

% Main definitions
filter_type = {'eskf','ekf'};   
ori_error = {'global','local'};
qint = {'zerof','zerob','first'};
imu_trunc = {1,2,3};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                         % Define Data type
                                                   %data.source = 'ROSBAG';
                                                       data.source = 'TXT';
                                                  
                                                        % Define data paths
          %                                folders{1} = 'data/simu/short/';
                                       %folders{1} = 'data/simu/straight/';
                                                folders{1} = 'data/simu/';
                                        %folders{1} = 'data/real/rosbags/';
                                        
                          % Random generator in case of simulation analysis
                                                      exp.rnd_enbl = false;
%__________________________________________________________________________

                                                       % Define filter type                                                    
                                               exp(1).met = filter_type{1};
                                               exp(1).frame = ori_error{1};
                                                 exp(1).qint_met = qint{3};
                                             exp(1).imu_met = imu_trunc{3};
                                                       exp(1).obs_rate = 1; 
                                                         exp(1).n_runs = 1;

                                               exp(2).met = filter_type{2};
                                               exp(2).frame = ori_error{1};
                                                 exp(2).qint_met = qint{1};
                                             exp(2).imu_met = imu_trunc{1};
                                                       exp(2).obs_rate = 1; 
                                                         exp(2).n_runs = 1;                                                          
                               
% % All filter types:
% exp_idx = 1;
% for met = filter_type
%   for frame = ori_error
%     for qint_met = qint
%         for imu_met = imu_trunc
%            fprintf('Set Up: %s,%s,%s,%d\n',met{1},frame{1},qint_met{1},imu_met{1}); 
%            exp(exp_idx).met = met{1};
%            exp(exp_idx).frame = frame{1};
%            exp(exp_idx).qint_met = qint_met{1};
%            exp(exp_idx).imu_met = imu_met{1};
%            exp(exp_idx).obs_rate = 1; % To reduce the correction step: 1 of each "obs_rate"
%            exp(exp_idx).g = [0;0;-9.803057]; %Barcelona gravity vector 
%            exp_idx = exp_idx+1;
%         end
%     end  
%   end  
% end                                                         
% __________________________________________________________________________

if strcmp(data.source,'ROSBAG')
                                                               % If needed,
                                                 % define ROSBAG file names 
                   bagfile_names{1} = 'front_back_2016-03-27-08-20-48.bag';
                               data.topics.gtodom = '/QuadrotorOscar/odom';
                                  data.topics.imu = '/quad_decode_msg/imu';
                                 data.topics.px4flow = '/px4flow/opt_flow';
                            data.topics.range = '/teraranger/timeofflight';
end                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                                                                                                                                  

%% MISC

if strcmp(data.source,'ROSBAG')    
    data.type = 'real';
    for ii=size(folders,2)
        data.paths{ii} = strcat(folders{ii},bagfile_names{ii});
    end   
elseif strcmp(data.source,'TXT')  
    for ii=1:size(folders,2)
        data.paths{ii} = folders{ii};
    end   
else
    error('Wrong data source type.');
end

return