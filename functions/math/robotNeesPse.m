% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Robot NEES and Position SE
% 
%   NEES = robotNees(Rob,GTRob)
% 
%   Robot's normalized estimation error squared and Position Squared Error.
%   Computes the NEES and SE of the estimated robot ROB wrt ground truth GTRob. 
%   The result is the NEES corresponding to the 6-DOF robot frame expressed in Euler angles.
%   And the Position Squared Error.
% 
%   - Inputs:
%       - robots:   All robots (Ground truth is first, then estimations).
%                   See newrobot.m for structure details.
% 
%   - Outputs:
%       - error:    Structure with All NEES and SE for all robots in path
%   
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [error] = robotNeesPse(robots)

GTRob = robots(1);

for nrob=2:size(robots,2)  
    error.names{nrob-1} = robots(nrob).name;

    Rob = robots(nrob);

    for ii = 2:size(Rob.t,2)
    
        % Check NAN values in Gtruth
        if(~any(isnan(GTRob.state(:,ii))))      
            
            % ground truth
            x_gt = qpose2epose([GTRob.state(1:3,ii);GTRob.state(7:10,ii)]); % to Euler
    
            if (~isempty(strfind(Rob.name,'EKF')))
                m = [Rob.state(1:3,ii);Rob.state(7:10,ii)];
                P = [Rob.P(1:3,1:3,ii) Rob.P(1:3,7:10,ii);
                     Rob.P(7:10,1:3,ii) Rob.P(7:10,7:10,ii)];      
                [m,P] = propagateUncertainty(m,P,@qpose2epose);
            else
                m = qpose2epose([Rob.state(1:3,ii);Rob.state(7:10,ii)]);
                P = [Rob.P(1:3,1:3,ii) Rob.P(1:3,7:9,ii);
                     Rob.P(7:9,1:3,ii) Rob.P(7:9,7:9,ii)];             
            end
        
            % estimation error
            e = x_gt-m;
            e(4:6) = normAngle(e(4:6));
            
            % normalized estimation error squared
            error.NEES(ii,nrob-1) = nees(e,0,P);
        
            % Error            
            error.E(ii,:,nrob-1) = e;
            
            % Orientation Error
            Rgt = q2R(GTRob.state(7:10,ii));
            Rest = q2R(Rob.state(7:10,ii));
            error.OE(ii,nrob-1) = 0.5*trace(eye(3) - Rgt'*Rest);
        else
            warning('NEES calculation: Ground truth contains NAN values.');
        end
    end
       
    
    % Trajectory Position RMSE
    error.PRMSE(1,nrob-1) = sqrt(mean(error.E(:,1,nrob-1).*error.E(:,1,nrob-1))); %x
    error.PRMSE(2,nrob-1) = sqrt(mean(error.E(:,2,nrob-1).*error.E(:,2,nrob-1))); %y
    error.PRMSE(3,nrob-1) = sqrt(mean(error.E(:,3,nrob-1).*error.E(:,3,nrob-1))); %z
    
    % Trajectory Orientation RMSE
    error.ORMSE(nrob-1) = sqrt(mean(error.OE(ii,nrob-1).*error.OE(ii,nrob-1)));
    
    % STD Position error
    error.STDP(1,nrob-1) = std(error.E(:,1,nrob-1));
    error.STDP(2,nrob-1) = std(error.E(:,2,nrob-1));
    error.STDP(3,nrob-1) = std(error.E(:,3,nrob-1));
    
    % STD Orientatio error
    error.STDO(1,nrob-1) = std(error.OE(:,nrob-1));

    if isempty(error.NEES)
        error('Error Computing NEES.'); 
    end

    if isempty(error.E)
        error('Error Computing E.'); 
    end
    
    if isempty(error.OE)
        error('Error Computing OE.'); 
    end
    
    error.obs_rate = Rob.obs_rate;
end

return