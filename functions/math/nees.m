% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
%   __________________________________________________________________
%
%   NEES
% 
%   [N] = nees(x,m,P)
% 
%   Normalized Estimation Error Squared.
%   Computes the Normalized Estimation Error Squared given true state X 
%   and a Gaussian estimate of mean M and covariance P.
% 
%   - Inputs:
%       - x:    True state ([pos euler])
%       - m:    Gaussian estimate mean
%       - P:    Gaussian estimate covariance
% 
%   - Outputs:
%       - N:    Normalized Estimation Error Squared
%   
%   Extracted from slamToolbox (Joan Sola @ iri.upc.edu). 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [N] = nees(x,m,P)

N = (x-m)'/P*(x-m);

return