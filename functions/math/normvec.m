% Copyright (C) by A. Santamaria-Navarro and Joan Sola (asantamaria@iri.upc.edu, jsola@iri.upc.edu) 
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Normalize vector.
%
%   [vn,VN_v]=normvec(v,jacMethod)
%
%   Returns the the unit length vector in the same direction and sense as
%   V (It is equal to V/norm(V)) and its jacobian if requested.
%
%   [NV,NV_v] = NORMVEC(V) returns the Jacobian of the normed vector wrt V.
%
%   [NV,NV_v] = NORMVEC(V,method) , with method ~= 0, uses a scalar diagonal
%   matrix as Jacobian, meaning that the vector has been just scaled to
%   length one by a scalar factor.
%   
%   Inputs:
%       - v:            Input vector to normalize.
%       - jacMethod:    Method to apply. 0: scalar. 1: full vector.
% 
%   Outputs:
%       - J_v:  Jacobian wrt input vector.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   Extracted from slamToolbox (Joan Sola @ iri.upc.edu).
%   __________________________________________________________________

function [vn,J_v]=normvec(v,jacMethod)

n2 = dot(v,v);
n  = sqrt(n2);

vn = v/n;

if nargout > 1
    s = numel(v);

    if nargin > 1 && jacMethod ~= 0  % use scalar method (approx)
        J_v = eye(s)/n;
    else                             % use full vector method (exact)
        n3  = n*n2;   
        J_v = (n2*eye(s) - v*v')/n3;
    end

end

return

%% Check with symbolic library (run section. If 0's, correct.)

syms v1 v2 v3 v4 v5 real
v = [v1;v2;v3;v4;v5];
[vn,VN_v] = normvec(v);

simplify(VN_v - jacobian(vn,v))