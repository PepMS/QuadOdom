% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Trajectory stadistics
% 
%   [error] = traj_stadistics(robots)
% 
%   - Inputs:
%       - paths:        Paths with the data (.mat files).
%       - plotparams:   Plot parameters.
% 
%   - Outputs:
%       - error:    Error analysis (rmse, anees, etc.). organized both in
%                   by paths or robot names. 
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function error = traj_stadistics(paths,plotparams)

for np = 1:size(paths,2)
    
    disp(strcat('...obtaining Robots from path: ',paths{np}));
    
    % Get all robots from specified path
    if strcmp(plotparams.source,'ROSBAG')
        robots = rosbag_wrapper_getrobots(paths{np},plotparams);
    else
        robots = getrobots_matfiles(paths{np},plotparams.rob);
    end    
    
    disp(strcat('...Synchronizing Robots from path: ',paths{np}));
    
    % Synchronize robots (same time of samples)
    for ii=2:size(robots,2)  
        robots(ii) = robotsync(robots(ii),robots(1));
    end
    
    disp(strcat('...Sorting Robots from path: ',paths{np}));
    
    % Order alphabetically
    robots = robot_sortalph(robots);

    disp(strcat('...Computing robotNEESPSE for path: ',paths{np}));
    error.path(np) = robotNeesPse(robots);
end 
% 
% % organize as robot types apart from paths            
% error.names_list = error.path(1).names; % Initialize list with first path names
% for ii = 1:size(error.path(1).names,2)
%     error.robot(ii).name = error.path(1).names(ii);
%     error.robot(ii).NEES{1} = error.path(1).NEES(:,ii);
%     error.robot(ii).E{1} = error.path(1).E(:,ii);
%     error.robot(ii).OE{1} = error.path(1).OE(:,ii);
%     error.robot(ii).PRMSE{1} = error.path(1).PRMSE(:,ii);
%     error.robot(ii).ORMSE{1} = error.path(1).ORMSE(:,ii);
%     error.robot(ii).STDP{1} = error.path(1).STDP(:,ii);
%     error.robot(ii).STDO{1} = error.path(1).STDO(:,ii);
% end
% % Fill with all other robots (rob) in all other paths (np)
% for np = 2:size(error.path,2) % Go over all paths (error dim = paths dim)
%     for rob = 1:size(error.path(np).names,2) % Go over all robots in path
%         pos_in_list = find(ismember(error.names_list,error.path(np).names{rob}),1);
%         if isempty(pos_in_list) % If not in the list, create a new type and add its data   
%             error.names_list{end+1} = error.path(np).names{rob};
%             error.robot(end+1).name = error.path(np).names{rob};
%             error.robot(end).NEES{1} = error.path(np).NEES(:,rob);
%             error.robot(end).E{1} = error.path(np).E(:,rob);
%             error.robot(end).OE{1} = error.path(np).OE(:,rob);
%             error.robot(end).PRMSE{1} = error.path(np).PRMSE(:,rob);
%             error.robot(end).ORMSE{1} = error.path(np).ORMSE(:,rob);
%             error.robot(end).STDP{1} = error.path(np).STDP(:,rob);
%             error.robot(end).STDO{1} = error.path(np).STDO(:,rob);
%         else  % If already in the list, add its data to the corresponding type
%             error.robot(pos_in_list).NEES{end+1} = error.path(np).NEES(:,rob);
%             error.robot(pos_in_list).E{end+1} = error.path(np).E(:,rob);
%             error.robot(pos_in_list).OE{end+1} = error.path(np).OE(:,rob);
%             error.robot(pos_in_list).PRMSE{end+1} = error.path(np).PRMSE(:,rob);
%             error.robot(pos_in_list).ORMSE{end+1} = error.path(np).ORMSE(:,rob);
%             error.robot(pos_in_list).STDP{end+1} = error.path(np).STDP(:,rob);
%             error.robot(pos_in_list).STDO{end+1} = error.path(np).STDO(:,rob);
%         end        
%     end           
% end
%         
% %Compute ANEES for each robot
% 
% % Get minimum common frames
% min_steps = size(error.robot(1).NEES{1},1);
% for nrob = 1:size(error.robot,2)
%     min_steps = min(min_steps,size(error.robot(nrob).NEES{1},1));
%     for exp = 2:size(error.robot(nrob).NEES,2)
%         min_steps = min(min_steps,size(error.robot(nrob).NEES{exp},1));
%     end    
% end
% 
% % get ANEES for each robot
% for nrob = 1:size(error.robot,2)
%    MatNESS = [];
%    for exp = 1:size(error.robot(nrob).NEES,2)
%        MatNESS = [MatNESS error.robot(nrob).NEES{exp}(1:min_steps,1)];
%    end
%    sumNEES(:,nrob) = sum(MatNESS,2); 
% end
% 
% error.t = robots(1).t;
% 
% numRuns = size(error.robot(1).NEES,2);
% 
% for nrob = 1:size(error.robot,2)
%     error.ANEES(:,nrob) = (1/numRuns)*sumNEES(:,nrob);
% end
% 
% dimX = 6; % Dimension corresponding to pose (position and orientation)
% DOF = dimX*numRuns;
% 
% hp=0.975;
% lp=0.025;
% Hchi2 = chi2(DOF,hp);
% Lchi2 = chi2(DOF,lp);
% Lnees = Hchi2/numRuns;
% Hnees = Lchi2/numRuns;
% 
% if plotparams.traj_stadistics.plot5comp
%     markers = {'x','o','d','*','s'};
%     legnames = {'A','B','C','D','E'};
%     marker_freq = floor(size(error.t,2)/15);
% end
% 
% col = lines(size(error.ANEES,2));
% 
% % Plot ANEES _______________________
% 
% fig=98;
% hfig = figure(fig);
% name='ANEES';
% fontsize = 30;
% xlabel('s');
% ylabel('Avg. NEES');
% 
% set_figure_params(hfig,name,fontsize,[0 0 1300 350]);
% 
% %lower and upper bounds
% h3 = area([0 robots(1).t(end)],[Hnees Hnees],'basevalue',Lnees,'ShowBaseLine','off','LineStyle','none','FaceColor', [0.9 0.9 0.9],'EdgeColor','none');
% for ii = 1:size(error.ANEES,2)
%     hanees(ii) = plot(robots(1).t(1:size(error.ANEES,1)),error.ANEES(:,ii),'LineWidth',2,'Color',col(ii,:));
%     if plotparams.traj_stadistics.plot5comp
%         hanees2(ii) = plot(robots(1).t(1:marker_freq:size(error.ANEES,1)),error.ANEES(1:marker_freq:end,ii),markers{ii},'LineWidth',2,'Color',col(ii,:),'MarkerSize',floor(fontsize/2));
%     end
% end
% 
% if plotparams.traj_stadistics.plot5comp
%     lanees=legend(hanees2,legnames);
% else
%     lanees=legend(hanees,strcat('$',error.names_list,'$'));
%     set(lanees,'interpreter','latex');
% end
% set(lanees,'Location','northeastoutside');
% legendmarkeradjust(30);
% hold off
% 
% % set axis
% 
% 
% 
% ymin = min(min(error.ANEES(:,:)));
% ymax = max(max(error.ANEES(:,:)));
% 
% ylim([ymin ymax])
% 
% % for ii = 2:size(error.ANEES,2)
% %     xmin = min(xmin min(error.ANEES(:,ii)) );
% %     xmax = max(xmax max(error.ANEES(:,ii)) );    
% % end
% 
% 
% % set(gca,'YTick', [2 4 10 20 40 100])
% 
% % set(gca,'YScale','log'); %// NEW
% 
% % Plot RMSE _______________________
% % fig=99;
% % hfig = figure(fig);
% % name='RMSE';
% % fontsize = 30;
% % xlabel('s');
% % ylabel('m');
% % 
% % set_figure_params(hfig,name,fontsize,[0 0 1300 350]);
% % 
% % %lower and upper bounds
% % for ii = 1:size(error.RMSE,2)
% %     hrmse(ii) = plot(robots(1).t(1:size(error.RMSE,1)),error.RMSE(:,ii),'LineWidth',2,'Color',col(ii,:));
% %     if plotparams.traj_stadistics.plot5comp
% %         hrmse2(ii) = plot(robots(1).t(1:marker_freq:size(error.RMSE,1)),error.RMSE(1:marker_freq:end,ii),markers{ii},'LineWidth',2,'Color',col(ii,:),'MarkerSize',floor(fontsize/2));
% %     end
% % end
% % 
% % if plotparams.traj_stadistics.plot5comp
% %     lrmse=legend(hrmse2,legnames);
% % else
% %     lrmse=legend(hrmse,strcat('$',error.names_list,'$'));
% %     set(lrmse,'interpreter','latex');
% % end
% % set(lrmse,'Location','northeastoutside');
% % legendmarkeradjust(30);
% % 
% % hold off
%         
return