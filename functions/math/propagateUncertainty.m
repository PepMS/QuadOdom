% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Non-linear propagation of Gaussian uncertainty.
% 
%   [ep,EPq] = qpose2epose(qp)
% 
%   Propagates the Gaussian uncertainty N(x,X) through function f(), 
%   resulting in the Gaussian approximation N(y,Y).
% 
%   - Inputs:
%       - x:        Mean.
%       - X:        Covariance.
%       - f:        Function.
% 
%   - Outputs:
%       - y:        Non-linear propagation of Gaussian uncertainty.
%       - Y:        Jacobian of the operation.
% 
%   Extracted from slamToolbox (Joan Sola @ iri.upc.edu). 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [y,Y] = propagateUncertainty(x,X,f)

[y,Y_x] = f(x);
Y = Y_x*X*Y_x';

return

%% test

x = randn(3,1)
X = randn(3)/1e5
[y,Y] = propagateUncertainty(x,X,@e2q)
[x,X] = propagateUncertainty(y,Y,@q2e)