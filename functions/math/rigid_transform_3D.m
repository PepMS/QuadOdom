% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   RIGID TRANSFORM 3D
% 
%   This function finds the optimal Rigid/Euclidean transform in 3D space.
%   It expects row data.
% 
%   - Inputs:
%       - A:    Nx3 matrix of 3D points.
%       - B:    Nx3 matrix of 3D points.
% 
%   - Outputs:
%       - R:    Rotations to aligna A and B.
%       - t:    Translation to align A and B.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [R,t] = rigid_transform_3D(A, B)

if nargin ~= 2
    error('Missing parameters');
end

assert(size(A,1) == size(B,1))
assert(size(A,2) == size(B,2))

centroid_A = mean(A);
centroid_B = mean(B);

N = size(A,1);

H = (A - repmat(centroid_A, N, 1))' * (B - repmat(centroid_B, N, 1));

[U,S,V] = svd(H);

R = V*U';

if det(R) < 0
%     fprintf('Reflection detected\n');
    V(:,3) = -V(:,3);
    R = V*U';
end

t = -R*centroid_A' + centroid_B';

end
