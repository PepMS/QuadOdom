% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Split vectors into scalars, or matrices into row vectors.
%
%   [s1,s2,...,sn] = SPLIT(V), with V a vector, returns all its components
%   in scalars s1 ... sn. It is an error if numel(V) < nargout.
%
%   [v1,...,vn] = SPLIT(M), with M a matrix, returns its rows as separate
%   vectors v1 ... vn. It is an error if size(M,2) < nargout.
%   
%   Inputs:
%       - A:    Vector or matrix
%
%   Outputs:
%       - s1..s2/v1..vn:    Scalar of the vector or rows of the matrix.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function varargout = split(A)

if min(size(A)) == 1
    % A is vector. We take row or column.
    ni = length(A);
    if nargout > ni
        error('not enough components in input vector');
    else
        for i = 1:nargout
            varargout{i} = A(i);
        end
    end
else
    % A is matrix. We split rows only.
    ni = size(A,2);
    if nargout > ni
        error('not enough rows in input matrix');
    else
        for i = 1:nargout
            varargout{i} = A(i,:);
        end
    end
end
