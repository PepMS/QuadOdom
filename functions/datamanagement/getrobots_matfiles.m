% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   GET ROBOTS
% 
%   [robots] = getrobots_matfiles(path,params)
% `
%   This functions reads all .MAT files stored in PATH and returns a
%   structure called ROBOTS. Each robot is either a filter estimation or the ground
%   truth.
% 
%   - Inputs:
%       - path:     Path where the estimation results are stored.
%       - params:   General plot parameters.
% 
%   - Outputs:
%       - robots:   Robots structure with estimation and ground truth data.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [robots] = getrobots_matfiles(path,params,obs_rate)

res_path = strrep(path, 'data', 'results');
listOfMats = dir([res_path '*.mat']);
numberOfMats = numel(listOfMats)-1; % -1: Sensor data is not a robot.
color = lines(numberOfMats+1);
size_m2m = params.size_m2m;
frame_len = params.frame_length;

for ii = 1:numberOfMats
    load([res_path listOfMats(ii).name])
    name = strrep(listOfMats(ii).name,'.mat','');    
    if ~isempty(strfind(listOfMats(ii).name,'gtruth'))
        robots(ii) = newrobot(name,color(1,:),tgt,gtruth,[],size_m2m,frame_len);
    else
        robots(ii) = newrobot(name,color(ii+1,:),tstate,xstate,P,size_m2m,frame_len,obs_rate);
    end
end

if ~exist('robots','var')
    error('Error reading .mat files. Could not get robots from path.');
end

return