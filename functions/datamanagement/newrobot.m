% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   NEW ROBOT
% 
%   Creates a generic robot structure
% 
%   - Inputs:
%       - name:         Robot name.
%       - color:        Robot color (1x3).
%       - t:            Time stamps.
%       - state:        State estimations.
%       - P:            Covariance matrices.
%       - r_quad:       Distance from motor to motor.
%       - frame_len:    Length of coordinate axis.
%       - obs_rate:     Observation rate.
% 
%   - Outputs:
%       - robot:    Robot structure.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function robot = newrobot(name,color,t,state,P,size_m2m,frame_len,obs_rate)

if nargin == 7 
    obs_rate = 1;
end

robot.name = name;
robot.color = color; 
robot.t = t;
robot.state = state;
robot.P =  P; 
robot.size_m2m = size_m2m;
robot.frame_len = frame_len;
robot.obs_rate = obs_rate;

return