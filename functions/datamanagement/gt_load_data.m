% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Load Ground truth data from .txt (real) or .mat (simulation) files.
%
%   [gtruth,time] = gt_load_data(path)
%
%   Returns the ground truth pose and twist for the robot.
%   
%   Inputs:
%       - path:    Path where the data is stored.
%
%   Ouputs:
%       - gtruth:  Pose measurement. [position velocity orientation angular_velocity](m m/s quaternion rad/s) 
%       - time:    Time stamp of readings starting at 0. (s)  
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [gtruth,time] = gt_load_data(path)

% Add path
warning ('off','all');
rmpath(genpath('data'));
warning ('on','all');
addpath(path);

gtruth = [];

if strfind(path, 'simu')
    disp('-> Loading simulation Ground Truth data...')    
    
    % Current .mat format as QuadSim quadrotor simulator.
    file = [path 'odom_gt.mat'];
    load(file);
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    time = odom_gt(:,1)';

    % Position
    p = odom_gt(:,2:4);
     
    % Orientation
    for ii=1:size(odom_gt,1)
       q(ii,:) = e2q(odom_gt(ii,5:7)); 
    end
    
    % Linear velocities
    v = odom_gt(:,8:10);
    
    % Angular velocities
    w = odom_gt(:,11:13);
    
    % Ground Truth Vector  
    gtruth = [p v q w]';  
    
else
    disp('-> Loading real Ground Truth data...')        

    % GT pose expressed in Global Frame
    file='gtruth_pose.txt';
    if(exist(file, 'file')==2)
        
        load(file);
        
        % TIME: measures stamps (starting point at 0, measures in secs) 
        time=gtruth_pose(:,3);  
        time=((time-time(1))*10^-9)';
        
        if size(gtruth_pose,2) == 10 % From ROS Pose message
            px = 4;
        elseif size(gtruth_pose,2) == 88 % From ROS Odometry message
            px = 6;
        else
            error('Wrong gtruth_pose.txt fields');
        end
        
        % Position
        x = gtruth_pose(:,px)';
        y = gtruth_pose(:,px+1)';
        z = gtruth_pose(:,px+2)';
        p = [-y' x' z']; % Frame rotation. Check with your robot.

        % Orientation    
        qx = gtruth_pose(:,px+3)';
        qy = gtruth_pose(:,px+4)';
        qz = gtruth_pose(:,px+5)';
        qw = gtruth_pose(:,px+6)';
        q = [qw' qx' qy' qz'];
            
        % Linear velocities
        vx = zeros(1,size(time,2));
        vy = zeros(1,size(time,2));
        vz = zeros(1,size(time,2));
        v = [vx' vy' vz'];
  
        for ii=2:size(time,2)
            dt = time(ii)-time(ii-1); 
            vx(ii) = (x(ii)-x(ii-1))/dt;
            vy(ii) = (y(ii)-y(ii-1))/dt;
            vz(ii) = (z(ii)-z(ii-1))/dt;
        end
        
        % Angular velocities
        w = zeros(size(time,2),3); % Currently not registered.
  
        % Ground Truth Vector  
        gtruth = [p v q w]';  
    end
        
end
    
return