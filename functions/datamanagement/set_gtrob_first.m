% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Set Ground Truth First
% 
%   [Rob] = set_gtrob_first(Rob)
% 
%   This functions orders the Robots setting the ground truth in the first
%   position
% 
%   - Inputs:
%       - Rob:      Robots to be ordered.
%   - Outputs:
%       - Rob:      Robots ordered.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Rob] = set_gtrob_first(Rob)

% Set ground truth as first robot
numgt = 0;
for ii=1:size(Rob,2)
    if ~isempty(strfind(Rob(ii).name,'gtruth'))
        Robtmp = Rob(1+numgt);
        Rob(1+numgt) = Rob(ii);
        Rob(ii) = Robtmp;
        numgt = numgt + 1;
    end    
end    

return