% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Get Path length
% 
%   [distime] = getpathlength(Rob)
% 
%   Return the absolute length of the path done by the robot.
% 
%   - Inputs:
%       - Rob:      Robot structure. See. 'newrobot.m'
% 
%   - Outputs:
%       - distime:  Distance done by the robot (ROB) r.t. time step. 
%                    - first row: time step.
%                    - second row: distance done.
%                    - third row: Boolen to show if there was data or not 
%                                 in that time step (Gorund truth carries 
%                                 NANs due to detection error).
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [distime] = getpathlength(Rob)

state = Rob.state;
t = Rob.t;
dis = 0;

distime(:,1) = [t(1);dis;0];

for ii=2:size(t,2)
    if(~(any(isnan(state(1:3,ii)))) && ~(any(isnan(state(1:3,ii-1)))))
        diff = state(1:3,ii)-state(1:3,ii-1);
        ddis = sqrt(diff'*diff);
        trow = 0;
    else
        ddis = 0; 
        trow = 1;
    end
    dis = dis + abs(ddis);
    distime(:,ii) = [t(ii);dis;trow];
end

end