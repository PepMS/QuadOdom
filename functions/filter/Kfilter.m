% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Kalman Filter
% 
%   This functions runs the Kalman filter specified in PARAMS. It uses the
%   IMU for propagation and Range and Flow readings for correction.
%
%   Please cite:
%   
%     A. Santamaria-Navarro, J. Solà and J. Andrade-Cetto. 
%     High-frequency MAV state estimation using low-cost inertial and 
%     optical flow measurement units, 2015 IEEE/RSJ International 
%     Conference on Intelligent Robots and Systems, 2015, Hamburg, 
%     pp. 1864-1871.
%
% 
%   - Inputs:
%       - params:   Filter type and parameters.
%       - xstate:   Initalized State vector.
%       - P:        Initialized System Covariance matrix.  
%       - Fi:       Jacobian of perturbations (maps IMU std dev).
%       - Qi:       Perturbation Matrix (diagonal of IMU std dev). 
%       - t_imu:    IMU time vector.
%       - a_s:      IMU acc. readings.
%       - w_s:      IMU gyro. readings.
%       - t_of:     Optical flow time vector.
%       - of_s:     Optical flow readings.
%       - t_range:  Range time vector.
%       - range_s:  Range readings.
% 
%   - Outputs:
%       - tstate:    Time at each step done by the filter (either propagation or correction)
%       - xstate:   State estimation done by the filter.
%       - P:        System Covariance.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [tstate,xstate,P]=Kfilter(params,xtrue,Ptrue,Fi,Qi,t_imu,a_s,w_s,t_px4flow,px4flow_s,px4flow_params,t_range,range_s,range_params)

% Sensor indices
idx_imu = 2; % Two first values needed for integration.
idx_px4flow = 1;
idx_range = 1;
idx_pose = 1;

% Time at each step done by the filter (either propagation or correction)
tstate = t_imu(1,1);

t_pose = t_imu(1,1); % Currently only used internally to have to the robot static at the beginning and end of the mission.
pose_s = [xtrue(1:3);q2e(xtrue(7:10))];
range_in_bounds = false;
xstate_at_stop = xtrue; 
xstate = xtrue;
P = Ptrue;

while idx_imu <= size(t_imu,2)
      
    % IMU time step
    dt_imu = t_imu(idx_imu)-t_imu(idx_imu-1);

    % Error initialized to 0 at every step
    dxstate = zeros(18,1);
    
    % Angular velocity
    ang_vel = w_s(:,idx_imu) - xtrue(14:16);
        
    
    % Virtual Pose sensor to keep robot static while no good sensors
    % updates are available
    if range_s(1,idx_range) < range_params.bounds.lower || px4flow_s(1,idx_px4flow) < px4flow_params.bounds.lower     
                      
        if range_in_bounds 
            range_in_bounds = false;
            xstate_at_stop = xtrue;
        end
                
        t_pose = [t_imu(idx_imu)]; % Correction after imu propagation      
        pose_s = [xstate_at_stop(1:2,1);xstate(3,end);q2e(xstate_at_stop(7:10,1))]; % Robot at 10cm height 
    else
        range_in_bounds = true;
    end     
    
    
    % Analize which sensor needs to be processed according to their time
    % and bounds
    sensor_names = {'imu'};
    sensor_times = [t_imu(idx_imu)];
    if idx_px4flow < size(t_px4flow,2)
        sensor_names(end+1) = {'px4flow'};
        sensor_times = [sensor_times t_px4flow(idx_px4flow)];
    end
    if idx_range < size(t_range,2)
        sensor_names(end+1) = {'range'};
        sensor_times = [sensor_times t_range(idx_range)];        
    end
    if idx_pose < size(t_pose,2)
        sensor_names(end+1) = {'pose'};
        sensor_times = [sensor_times t_pose(idx_pose)];        
    end
    [sensor] = which_next(sensor_names,sensor_times);    
        
    % Initialize correction flag
    sensor_correction = false;    
    
    switch sensor
        case 'imu'           
                      
            % Prediction __________________________________________________
            %Mean State Prediction
            [xtrue] = mean_predict(xtrue,a_s(:,idx_imu-1:idx_imu),w_s(:,idx_imu-1:idx_imu),dt_imu,params.qint_met,params.imu_met);

            %IMU Transition matrix
            [F_xstate,F_dxstate] = imu_trans_mat(xtrue,dt_imu,a_s(:,idx_imu),w_s(:,idx_imu),params.frame,params.imu_met);

            %Covariance Prediction  
            switch lower(params.met)
                case {'ekf'}
                    [~,Fn] = EKFjacobians(xtrue,dt_imu);
                    [Ptrue] = cov_predict(Ptrue,F_xstate,Fn,Qi); 
                case {'eskf'}
                    [Ptrue] = cov_predict(Ptrue,F_dxstate,Fi,Qi);
            end            
                                                         
            % Increment counters
            tstate = [tstate t_imu(idx_imu)];
            idx_imu = idx_imu + 1;         
            xstate(:,end+1) = xtrue;
            P(:,:,end+1) = Ptrue; 
                       
        case 'px4flow'  
            if (idx_imu >= 3) % First prediction done
                [xest,dxest,Pest,tstate,sensor_correction] = px4flow_correction(tstate,xtrue,dxstate,Ptrue,ang_vel,params,px4flow_params,t_px4flow(idx_px4flow),px4flow_s(:,1:idx_px4flow));
            end
            % Increment counter
            idx_px4flow = idx_px4flow + 1;
            
        case 'range'
            if (idx_imu >= 3) % First prediction done
                [xest,dxest,Pest,tstate,sensor_correction] = range_correction(tstate,xtrue,dxstate,Ptrue,params,range_params,t_range(idx_range),range_s(:,idx_range),idx_range);
            end
            % Increment counter
            idx_range = idx_range + 1;
        case 'pose'  
             if (idx_imu >= 3) % First prediction done
                [xest,dxest,Pest,tstate,sensor_correction] = pose_correction(tstate,xtrue,dxstate,Ptrue,params,t_pose(idx_pose),pose_s(:,idx_pose));
             end                                                                                 
            % Increment counter
            idx_pose = idx_pose + 1;           
    end
    
    if sensor_correction
        switch lower(params.met)
            case {'eskf'}
                %_____________ Update _____________________________________
                [xtrue] = ESKFupdate(xtrue,dxest,params.frame);
                               
                %_____________ Reset ______________________________________
                [~,Ptrue] = ESKFreset(dxest,Pest,params.frame);
            case {'ekf'}
                %_____________ Reset ______________________________________
                [xtrue,Ptrue] = EKFreset(xest,Pest);
        end 
        xstate(:,end+1) = xtrue;
        P(:,:,end+1) = Ptrue;
    end  
end

return