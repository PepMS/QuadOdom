% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Kalman Filter Innovation
%
%   [dz,Z]=innovation(y,e,P_old,H,Vof)
%
%   Computes innovation z and innovation's covariances matrix Z 
%   from a measurement N{y,R} to the expectation N{e,E} as
%
%     dz = y - e
%     Z = H * P_old * H' + Vof
%   
%   Inputs:
%       - y:        Measurement.
%       - e:        Expectation.
%       - P_old:    Covariance matrix at time k-1.
%       - H:        Observation Jacobian.
%       - Q:        Observation covariance matrix.   
% 
%   Outputs:
%       - dz:   Residual or innovation.
%       - Z:    Residual or innovation covariance matrix.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [dz,Z]=innovation(y,e,P_old,H,Q)

%Innovation
dz  = y - e;

%Covariance of the residual
Z = H * P_old * H' + Q;
Z= (Z+Z')/2;

return