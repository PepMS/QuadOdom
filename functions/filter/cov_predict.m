% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Covariance Prediction
%
%   [P]=cov_predict(P_old,Fx,Fi,Qi)
%
%   Returns the prediction step for the error-state vector and the new
%   covariance.
%   
%   Inputs:
%       - P_old:        Covariance matrix at time k-1.
%       - Fx:           ESKF Transition matrix.
%       - Fi:           Jacobian that maps the IMU covariance.
%       - Qi:           Imu covariance matrix.
% 
%   Outputs:
%       - P:            Covariance matrix at time k.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   _____________________

function [P]=cov_predict(P_old,Fx,Fi,Qi)

%Covariance matrix
P = Fx*P_old*Fx'+ Fi*Qi*Fi';
P = (P + P')/2; %force symmetric

return