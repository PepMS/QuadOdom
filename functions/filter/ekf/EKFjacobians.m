% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   EKFJacobians
% 
%   This function computes the Control (Fu) and Perturbation Jacobians
% 
%   [Fu,Fn] = EKFjacobians(xstate,dt)
% 
%   Inputs:
%       - xstate:   Nominal state vector.
%       - dt:       Time differential.
% 
%   Outputs:
%       - Fu:       Control Jacobian.
%       - Fn:       Perturbation Jacobian.
%   __________________________________________________________________

function [Fu,Fn] = EKFjacobians(xstate,dt)

q = xstate(7:10);
R = q2R(q);
Quatomega = 0.5*q2Pi(q);    

% Control Jacobian
Fu = [0.5*R*dt^2    zeros(3);
      R*dt          zeros(3);
      zeros(4,3)    Quatomega;
      zeros(3)      zeros(3);
      zeros(3)      zeros(3);
      zeros(3)      zeros(3)];

% Perturbation Jacobian
Fn = [ -0.5*R         zeros(3)      zeros(3)     zeros(3);
       -R             zeros(3)      zeros(3)     zeros(3);
       zeros(4,3)    -Quatomega     zeros(4,3)   zeros(4,3);
       zeros(3)       zeros(3)      eye(3)       zeros(3);
       zeros(3)       zeros(3)      zeros(3)     eye(3);
       zeros(3)       zeros(3)      zeros(3)     zeros(3)];
end

