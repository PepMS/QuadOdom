% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Range covariance matrix
%
%   [Vr] = range_cov(range_s,params)
%
%   Sensor covariance may vary depending sensor range bounds.
%   
%   Inputs:
%       - range_s:   Sensor reading.
%       - params:    Sensor parameters.
% 
%   Outputs:
%       - range_est_std:    Current std.
%       - Vr:               Range sensor covariance matrix.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [range_est_std,Vr] = range_cov(range_s,params)

% Set reading and covariance depending on sensor bounds
if (range_s(1,1)<params.bounds.lower)
  range_est_std = 0.1; % We assume robot landed.                   
elseif (range_s(1,1)>params.bounds.lower && range_s(1,1)<params.bounds.upper)
  range_est_std = params.std.insidebouds;
elseif (range_s(1,1)>params.bounds.upper)
  range_est_std = params.std.outsidebouds;
end

Vr = diag(range_est_std.^2);

return