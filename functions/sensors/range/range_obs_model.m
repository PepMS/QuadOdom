% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Range sensor observation model and Jacobian matrix.
%
%   [h,H_xstate,H_dxstate] = range_obs_model(xstate,frame,std_range)
%
%   Returns the Observation Jacobian from current nominal state vector
%   
%   Inputs:
%       - state:        State vector.
%       - frame:        Frame where the orientation error is expressed.
%       - params:       Sensor parameters.
%       - std_range:    Noise std dev.
%
%   Ouputs:
%       - h:         Sensor readings
%       - H_xstate:  Observation Jacobian w.r.t. nominal state (EKF)
%       - H_dxstate: Observation Jacobian w.r.t. error state (ESKF)
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [h,H_xstate,H_dxstate] = range_obs_model(xstate,frame,params,std_range)

if nargin == 1
    frame='local';
    params.model = 'sonar';    
    std_range=0.0;
elseif nargin == 2
    params.model = 'sonar';
    std_range=0.0; 
elseif nargin == 3
    std_range=0.0;     
end

Sz = [0 0 1];
p = xstate(1:3,1);
    
if strcmp(params.model,'sonar')
    
    % Observation model  
    h = Sz*p + randn(1,1).*std_range;   
    
    % Observation Jacobian 
    if nargout > 2
        H_dxstate = [Sz zeros(1,15)];
        H_xstate = [Sz zeros(1,16)];
    end
    
elseif strcmp(params.model,'time-of-flight')
        
    % Observation model
    [qc,QC_q] = q2qc(xstate(7:10,1));
    R_trans = q2R(qc);

    h = Sz*R_trans*p + randn(1,1).*std_range;

    % Observation Jacobian 
    if nargout > 2

        switch lower(frame)
            case 'local'
                Hr = Sz*vec2skew(R_trans*p);
            case 'global'
                Hr = Sz*R_trans*vec2skew(p);        
        end    

        Hp = Sz*R_trans;    
    
        % Jacobian r.t. Error__________________________
        H_dxstate = [Hp zeros(1,3) Hr zeros(1,9)];
    
        % Jacobian r.t. State__________________________
        [~, ~, W_qc] = qRot(p,qc);
        W_q =  W_qc * QC_q;
        Hr = Sz*W_q;    
        H_xstate = [Hp zeros(1,3) Hr zeros(1,9)];  
    end
    
else
    error('Wrong RANGE sensor type.');
end

return
