% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Range sensor correction
% 
%   This script performs a correction to the filter estimation using
%   Range readings.
% 
%   - Inputs:
%       - tstate:               Filter time vector.
%       - xstate:               Nominal State vector.
%       - dxstate:              Error State vector.
%       - P:                    System covariance matrix.
%       - ang_vel:              Angular velocity (without biases).
%       - params:               Filter parameters.
%       - range_params:         Range sensor parameters.
%       - t_range:              Range time vector.
%       - range_s:              Range readings.
%       - idx_range:            Current range reading (index).
% 
%   - Outputs:
%       - xstate:               Nominal State vector (Corrected).
%       - dxstate:              Error State vector (Corrected).
%       - P:                    System covariance matrix (Corrected).    
%       - tstate:               Filter time vector (Increased).
%       - sensor_correction:    Sensor correction flag.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xest,dxest,Pest,tstate,sensor_correction] = range_correction(tstate,xtrue,dxstate,Ptrue,params,range_params,t_range,range_s,idx_range)
    
xest = xtrue;
dxest = dxstate;
Pest = Ptrue;

% Observation covariance matrix
[std,Vr] = range_cov(range_s(:,1),range_params);     

%Observation Jacobian
if params.rnd_enbl
    [h,H_xstate,H_dxstate] = range_obs_model(xtrue,params.frame,range_params,std);
else
    [h,H_xstate,H_dxstate] = range_obs_model(xtrue,params.frame,range_params);
end
                
% Update with virtual values. The robot is considered landed.
if (range_s(1,1)<range_params.bounds.lower)                  
    range_s(1,1) = 0.0;
end
                   
  
           
sensor_correction = false;

% Counter to select when correction will be executed
if (mod(idx_range,params.obs_rate)==0) && (range_s(:,1)>range_params.bounds.lower) && (range_s(:,1)<range_params.bounds.upper)
    
    switch lower(params.met)
        case {'eskf'}
            [dz,Z] = innovation(range_s(:,1),h,Ptrue,H_dxstate,Vr);
                            
            %Check innovation with mahalanobis distance
            %if (mahalanobis(of_s(:,idx_of),h(:,ii),Z(:,:)) < 3)
            [dxest,Pest] = correction(dxstate,dz,Z,H_dxstate,Ptrue);
            %end            
        case {'ekf'}
%             [dz,Z] = innovation(range_s(:,1),h,Ptrue(:,:,end),H_xstate,Vr);
            [dz,Z] = innovation(range_s(:,1),h,Ptrue,H_xstate,Vr);
            [xest,Pest] = correction(xtrue,dz,Z,H_xstate,Ptrue);
    end
    tstate = [tstate t_range];
    sensor_correction = true;
end   

return
