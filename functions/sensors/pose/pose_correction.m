% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Pose sensor correction
% 
%   This script performs a correction to the filter estimation using
%   Pose readings.
% 
%   - Inputs:
%       - tstate:               Filter time vector.
%       - xstate:               Nominal State vector.
%       - dxstate:              Error State vector.
%       - P:                    System covariance matrix.
%       - params:               Filter parameters.
%       - t_pose:               Pose time vector.
%       - pose_s:               Pose readings.
% 
%   - Outputs:
%       - xstate:               Nominal State vector (Corrected).
%       - dxstate:              Error State vector (Corrected).
%       - P:                    System covariance matrix (Corrected).    
%       - tstate:               Filter time vector (Increased).
%       - sensor_correction:    Sensor correction flag.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xest,dxest,Pest,tstate,sensor_correction] = pose_correction(tstate,xtrue,dxstate,Ptrue,params,t_pose,pose_s)
 
xest = xtrue;
dxest = dxstate;
Pest = Ptrue;

% Observation covariance matrix
pose_est_std = 1e-1*ones(6,1);
Vp = diag(pose_est_std.^2);  

%Observation Jacobian
if params.rnd_enbl
    [h,H_xstate,H_dxstate] = pose_obs_model(xtrue,pose_est_std);
else
    [h,H_xstate,H_dxstate] = pose_obs_model(xtrue);    
end    
                                            
switch lower(params.met)
    case {'eskf'}
        [dz,Z] = innovation(pose_s,h,Ptrue,H_dxstate,Vp);
                            
        %Check innovation with mahalanobis distance
        %if (mahalanobis(pose_s,h,Z) < 3)
        [dxest,Pest] = correction(dxstate,dz,Z,H_dxstate,Ptrue);
        %end            
    case {'ekf'}
        [dz,Z] = innovation(pose_s,h,Ptrue,H_xstate,Vp);
        [xest,Pest] = correction(xtrue,dz,Z,H_xstate,Ptrue); 
end
tstate = [tstate t_pose];
sensor_correction = true;

return