% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   IMU Inertial Measurement Unit sensor model.
%
%   [y,Y_dxstate]=imu(state,a,w,std_sim_imu,dt)
%
%   Returns a 6-vector of 3-acc and 3-gyro measurements
%   corresponding to a moving body in a gravity field. The body has
%   orientation given by a quaternion in the state vector, 
%   with acceleration A and angular rate W. Acceleration is measured in 
%   the inertial frame where the gravity field is defined.
%
%   
%   Inputs:
%       - state:        State vector.
%       - a:            Acceleration (measured in the inertial frame where the 
%                       Gravity field is defined).
%       - w:            Angular rate (measured in the body frame).
%       - std_sim_imu:  Std dev. for the sensor.
%       - dt:           Time differential.
%       - frame:        Frame whether the orientation error is expressed.
% 
%   Outputs:
%       - y:            6-vector of 3-acc and 3-gyro measurements [am wm]'.
%       - Y_dxstate:    IMU transition matrix. Jacobian r.t. error (dxstate).
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [y,Y_dxstate]=imu_obs_model(xstate,a,w,std_sim_imu,dt,frame)

switch nargin
    case 6
    otherwise
        error('Wrong number of arguments.')
end

% rotation from quaternion
R = q2R(qNorm(xstate(7:10)));

% Measurements w/o noise
am = R'*(a - xstate(17:19)) + xstate(11:13);
wm = w + xstate(14:16);

% Measurements w noise.
am = am + randn(3,1).*std_sim_imu(1:3);
wm = wm + randn(3,1).*std_sim_imu(4:6);

y = [am ; wm];

% IMU transition matrix
if nargout > 1
    Y_dxstate = imu_trans_mat(am,wm,xstate,dt,frame);
end

return