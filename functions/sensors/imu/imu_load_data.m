% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Load IMU data from .txt (real) or .mat (simulation) files.
%
%   [w_s,Qw,a_s,Qa,time] = load_imu_data(path)
%
%   Returns the sensor measurements from compass, gyros and accelerometers.
%   The Covariance matrices will be loaded only if requested.
%   
%   Inputs:
%       - path: Path where the data is stored.
%
%   Ouputs:
%       - q_s:  Quaternion orientation. COMPASS readings.
%       - w_s:  3DOF Angular velocities. GYRO readings. (rad/s)
%       - a_s:  3DOF Linear accelerations. ACC. readings. (m/s)
%       - time: Time stamp of sensor readings starting at 0.  (s)
%       - Qq:   Covariance matrix fo q_s, read from file.
%       - Qw:   Covariance matrix of w_s, read from file.
%       - Qa:   Covariance matrix of a_s, read from file.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [q_s,w_s,a_s,time,Qq,Qw,Qa] = imu_load_data(path)

% Add path
warning ('off','all');
rmpath(genpath('data'));
warning ('on','all');
addpath(path);


if strfind(path, 'simu')
    disp('-> Loading simulation IMU data...')
    
    file = [path 'imu.mat'];
    load(file);
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    time = imu(:,1)';
    
    % COMPASS: Magnetic orientation measured in Body frame
    q_s = []; % Currently not implemented in simulation.
    Qq = []; % Currently not implemented in simulation.
    
    % GYROS: Angular vel. measured in Body frame
    w_s = imu(:,5:7)';
    Qw = []; % Currently not implemented in simulation.
    
    % ACCELEROMETERS: Linear acc. measured in Body frame
    a_s = imu(:,2:4)';
    Qa = []; % Currently not implemented in simulation.
    
else
    disp('-> Loading real IMU data...')
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    file='imu_header.txt';
    if(exist(file, 'file')==2) 
        load(file);
        time=imu_header(:,3); 
        time=((time-time(1))*10^-9)';
    end
    
    % COMPASS: Magnetic orientation measured in Body frame
    file='orientation.txt';
    if(exist(file, 'file')==2) 
        load(file);
        q_ros = orientation(:,2:end)';
        q_s = [q_ros(4,:);q_ros(1:3,:)]; %ROS has qw at the end
    end
    %Covariance
    file='orientation_covariance.txt';
    if(exist(file, 'file')==2 && nargout == 5) 
        load(file);
        Qq = orientation_covariance(:,2:end)';
    end

    % GYROS: Angular vel. measured in Body frame
    file='angular_velocity.txt';
    if(exist(file, 'file')==2)
        load(file);
        w_s = angular_velocity(:,2:end)';
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    %     time=angular_velocity(:,1); 
    %     time=((time-time(1))*10^-9)';
    end
    %Covariance
    file='angular_velocity_covariance.txt';
    if(exist(file, 'file')==2 && nargout == 6)
       load(file);    
       Qw = angular_velocity_covariance(:,2:end)';
    end

    % ACCELEROMETERS: Linear acc. measured in Body frame
    file='linear_acceleration.txt';
    if(exist(file, 'file')==2)
       load(file);
       a_s = linear_acceleration(:,2:end)';
    end
    %Covariance
    file='linear_acceleration_covariance.txt';
    if(exist(file, 'file')==2 && nargout == 7)
       load(file);
       Qa = linear_acceleration_covariance(:,2:end)';
    end
    
end


return