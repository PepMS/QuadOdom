% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   IMU transition matrix
%
%   [F_xstate,F_dxstate] = imu_trans_mat(a_s,w_s,xstate,dt,frame,trunc_met)
%
%   Returns the IMU transition matrix for Model propagation. 
%   
%   Inputs:
%       - xstate:       Nominal State vector.
%       - dt:           Time differential.
%       - a_s:          Acc. readings.
%       - w_s:          Gyro. readings.
%       - frame:        Frame where the orientation error is expressed.
%       - trunc_met:    Transition matrix truncation grade.
% 
%   Outputs:
%       - F_xstate:     IMU transition matrix from nominal state (EKF)  
%       - F_dxstate:    IMU transition matrix from error state (ESKF) 
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [F_xstate,F_dxstate] = imu_trans_mat(xstate,dt,a_s,w_s,frame,trunc_met)

% Derivative r.t. Error-State _____________________________________________

q = xstate(7:10);
ab = xstate(11:13);
wb = xstate(14:16);

R = q2R(q);
Rw = vec2R((w_s-wb)*dt);

Pv = eye(3);
Va=-R;
Vg=eye(3);

switch lower(frame)
    case 'local'
        sk_a = vec2skew(a_s-ab);
        Vtheta=-R*sk_a;
        Thetatheta=vec2skew(w_s-wb);
        Trunc_sigma0=Rw';
        Thetaomega=-eye(3);
    case 'global'
        sk_Ra = vec2skew(R*(a_s-ab));
        Vtheta=-sk_Ra;
        Thetatheta=zeros(3);
        Trunc_sigma0=eye(3);
        Thetaomega=-R;
end

if trunc_met == 1 % Truncated n=1 (Euler)
    Trunc_sigma1 = eye(3)*dt;
    Trunc_sigma2 = zeros(3);
    Trunc_sigma3 = zeros(3);
elseif trunc_met == 2 % Truncated n=2 
    Trunc_sigma1=eye(3)*dt+0.5*Thetatheta*dt^2;
    Trunc_sigma2=0.5*eye(3)*dt^2;
    Trunc_sigma3=zeros(3);
elseif trunc_met == 3 % Truncated n=3
    Trunc_sigma1=eye(3)*dt+0.5*Thetatheta*dt^2+(1/6)*Thetatheta^2*dt^3;
    Trunc_sigma2=0.5*eye(3)*dt^2+(1/6)*Thetatheta*dt^3;
    Trunc_sigma3=(1/6)*eye(3)*dt^3;        
else
    disp('Wrong number of inputs in IMU error-state Transition matrix');
end

F_dxstate = [eye(3)    Pv*dt       Pv*Vtheta*Trunc_sigma2      0.5*Pv*Va*dt^2      Pv*Vtheta*Trunc_sigma3*Thetaomega   0.5*Pv*Vg*dt^2;
             zeros(3)  eye(3)      Vtheta*Trunc_sigma1         Va*dt               Vtheta*Trunc_sigma2*Thetaomega      Vg*dt;
             zeros(3)  zeros(3)    Trunc_sigma0                zeros(3)            Trunc_sigma1*Thetaomega             zeros(3);
             zeros(3)  zeros(3)    zeros(3)                    eye(3)              zeros(3)                            zeros(3);
             zeros(3)  zeros(3)    zeros(3)                    zeros(3)            eye(3)                              zeros(3);
             zeros(3)  zeros(3)    zeros(3)                    zeros(3)            zeros(3)                            eye(3)];       

% Derivative r.t. Nominal-State ___________________________________________

Quatquat = w2omega(w_s-wb);
Trunc_sigma0 = eye(4) + 0.5*dt*Quatquat;
Quatomega = -q2Pi(q);
[~, Vab, Vq] = qRot((a_s-ab),q);
Vab = -Vab;

if trunc_met == 1 % Truncated n=1 (Euler)
    Trunc_sigma1 = eye(4)*dt;
    Trunc_sigma2 = zeros(4);
    Trunc_sigma3 = zeros(4);
elseif trunc_met == 2 % Truncated n=2        
    Trunc_sigma1 = eye(4)*dt+0.5*Quatquat*dt^2;        
    Trunc_sigma2 = 0.5*eye(4)*dt^2;
    Trunc_sigma3 = zeros(4);
elseif trunc_met == 3 % Truncated n=3
    Trunc_sigma1=eye(4)*dt+0.5*Quatquat*dt^2+(1/6)*Quatquat^2*dt^3;
    Trunc_sigma2=0.5*eye(4)*dt^2+(1/6)*Quatquat*dt^3;
    Trunc_sigma3=(1/6)*eye(4)*dt^3;       
else
    disp('Wrong number of inputs in IMU error-state Transition matrix');
end

F_xstate = [eye(3)      Pv*dt         Pv*Vq*Trunc_sigma2      0.5*Pv*Vab*dt^2     Pv*Vq*Trunc_sigma3*Quatomega   0.5*Pv*Vg*dt^2;
            zeros(3)    eye(3)        Vq*Trunc_sigma1         Vab*dt              Vq*Trunc_sigma2*Quatomega      Vg*dt;
            zeros(4,3)  zeros(4,3)    Trunc_sigma0            zeros(4,3)          Trunc_sigma1*Quatomega         zeros(4,3);
            zeros(3)    zeros(3)      zeros(3,4)              eye(3)              zeros(3)                       zeros(3);
            zeros(3)    zeros(3)      zeros(3,4)              zeros(3)            eye(3)                         zeros(3);
            zeros(3)    zeros(3)      zeros(3,4)              zeros(3)            zeros(3)                       eye(3)];     
return