% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Initial IMU calibration (warm-up period)
%
%   [w_b,a_b,Qw_ini,Qa_ini]=imu_ini_calib(n0,w_s,a_s,g)
%
%   Initial warm-up period to calibrate the IMU values. Initial sensor
%   reading when the robot is not moving in an horizontal pose in the
%   floor.
%   
%   Inputs:
%       - n0: Initial sensor readings for warm-up period.
%       - w_s:  3DOF Angular velocities. GYRO readings.
%       - a_s:  3DOF Linear accelerations. ACC. readings.
%       - g:    Gravity vector where the data was collected.
% 
%   Outputs:
%       - w_b:      Initial Angular velocity bias
%       - a_b:      Initial Acceleration Bias
%       - Qimu_ini: Covariance matrix of IMU readings (12x12).
%       - Fi:       Mapping matrix for IMU covariance Fi*Qimu*Fi'.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [w_b,a_b,Qimu_ini,Fi]=imu_ini_calib(n0,w_s,a_s,g,dt)

fprintf('\n-> Initial calibration. Warm-up period of: ')
fprintf(num2str(n0))
fprintf(' sensor readings.\n');

% Initial Angular velocity bias
w_b(:,1)=mean(w_s(:,1:n0),2);

% Initial Acceleration Bias (gravity extraction depending on acc_g sign)
if(a_s(3,1)<0) 
    a_b(:,1) = mean(a_s(:,1:n0),2)-g;
else
    a_b(:,1) = mean(a_s(:,1:n0),2)+g;
end

% Initial covariance calibration during warm-up period 
Qw_ini = var(w_s(:,1:n0),0,2);
Qa_ini = var(a_s(:,1:n0),0,2);
Qimu_ini(1:3,1:3)=diag(Qa_ini)*dt;
Qimu_ini(4:6,4:6)=diag(Qw_ini)*dt;
Qimu_ini(7:9,7:9)=diag(a_b(:,1))*dt;
Qimu_ini(10:12,10:12)=diag(w_b(:,1))*dt;

% Qimu_ini=diag(1e-3)*dt;

Fi=zeros(18,12);
Fi(4:15,:)=eye(12);

return