% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Load Optical FLow data from .txt (real) or .mat (simulation) files.
%
%   [of_s,of_qual] = px4flow_load_data(path)
%
%   Returns the sensor measurements from PX4-optical flow sensor.
%   More info:  http://pixhawk.org/modules/px4flow
%   
%   Inputs:
%       - path:     Path where the data is stored.
%
%   Ouputs:
%       - of_s:         Sensor measurement [range;flow_uv;velocity_xy] (m;pixels/s;m/s)
%       - time:         Time stamp of sensor readings starting at 0.  (s)
%       - of_params:    Particular parameters of the sensor.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [px4flow_s,time] = px4flow_load_data(path)

% Add path
warning ('off','all');
rmpath(genpath('data'));
warning ('on','all');
addpath(path);

if strfind(path, 'simu')
    disp('-> Loading simulation PX4Flow data...')    
    
    file = [path 'px4flow.mat'];
    load(file);
    
    % TIME: measures stamps (starting point at 0, measures in secs)
    time = px4flow(:,1)';

    % RANGE
    range = px4flow(:,2)';

    % Flow2D
    flow = px4flow(:,6:7)';

    % LINEAR X VEL 
    vxy = px4flow(:,8:9)';    
        
else
    disp('-> Loading real PX4Flow data...')        

    % PX4 Optical Flow sensor output measured in Sensor frame 
    file='opt_flow.txt';
    if(exist(file, 'file')==2) 
        load(file);
        data = opt_flow(:,1:end)';
        
        % TIME: measures stamps (starting point at 0, measures in secs)
        time=data(3,:)'; 
        time=((time-time(1))*10^-9)';

        % RANGE
        range = data(4,:);

        % Flow2D
        flow = [data(5,:);data(6,:)];

        % LINEAR X VEL 
        vxy = [data(7,:);data(8,:)];

        % QUALITY RATIO
        %of_qual = data(9,:)'; % Currently not used  
    end
end

% READING
px4flow_s=[range;flow;vxy];
    
% Outlier DETECTION -> Outliers set to NAN
[px4flow_s(1,:)] = outliers(px4flow_s(1,:), 10000);

return