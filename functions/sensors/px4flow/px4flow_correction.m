% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   PX4Flow sensor correction
% 
%   This script performs a correction to the filter estimation using
%   PX4Flow readings.
% 
%   - Inputs:
%       - tstate:               Filter time vector.
%       - xstate:               Nominal State vector.
%       - dxstate:              Error State vector.
%       - P:                    System covariance matrix.
%       - ang_vel:              Angular velocity (without biases).
%       - params:               Filter parameters.
%       - px4flow_params:       PX4Flow sensor parameters.
%       - t_px4flow:            PX4Flow time vector.
%       - px4flow_s:            PX4Flow readings.
% 
%   - Outputs:
%       - xstate:               Nominal State vector (Corrected).
%       - dxstate:              Error State vector (Corrected).
%       - P:                    System covariance matrix (Corrected).    
%       - tstate:               Filter time vector (Increased).
%       - sensor_correction:    Sensor correction flag.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [xest,dxest,Pest,tstate,sensor_correction] = px4flow_correction(tstate,xtrue,dxstate,Ptrue,ang_vel,params,px4flow_params,t_px4flow,px4flow_s)

xest = xtrue;
dxest = dxstate;
Pest = Ptrue;

% Observation covariance matrix
[std,~,Vo,Vvxy] = px4flow_cov(px4flow_s(:,end),px4flow_params);  

%Observation Jacobian
if params.rnd_enbl
    [h,H_xstate,H_dxstate] = px4flow_obs_model(xtrue,params.frame,px4flow_params,ang_vel,std);
else
    [h,H_xstate,H_dxstate] = px4flow_obs_model(xtrue,params.frame,px4flow_params,ang_vel); 
end

% Observation Outlier removal
px4flow_s(:,end) = px4flow_est_vxy(px4flow_s,xtrue,ang_vel,'estimate'); 
       
% Update with virtual values. The robot is considered landed.
if (px4flow_s(1,end) < px4flow_params.bounds.lower)                  
    px4flow_s(:,end) = [0.0;0.0;0.0;0.0;0.0];
end    

sensor_correction = false;

% Counter to select when correction will be executed
if (mod(size(px4flow_s,2),params.obs_rate)==0) && (px4flow_s(1,end)>px4flow_params.bounds.lower) && (px4flow_s(1,end)<px4flow_params.bounds.upper)
    
    % Choose data and covariance depending on Sensor Datatype                    
    if strcmp(px4flow_params.datatype,'flow2d')
        px4flow_short = [px4flow_s(1,end);px4flow_s(2:3,end)];
        px4flow_covmat = Vo;                    
    elseif strcmp(px4flow_params.datatype,'vxy')
        px4flow_short = [px4flow_s(1,end);px4flow_s(4:5,end)];
        px4flow_covmat = Vvxy;                           
    end
                                       
    switch lower(params.met)
        case {'eskf'}
            [dz,Z] = innovation(px4flow_short,h,Ptrue,H_dxstate,px4flow_covmat); % Only using range and Vxy 
                            
            %Check innovation with mahalanobis distance
            %if (mahalanobis(of_s(:,idx_of),h(:,ii),Z(:,:)) < 3)
            [dxest,Pest] = correction(dxstate,dz,Z,H_dxstate,Ptrue);
            %end                           
        case {'ekf'}
            [dz,Z] = innovation(px4flow_short,h,Ptrue,H_xstate,px4flow_covmat); % Only using range and Vxy
            [xest,Pest] = correction(xtrue,dz,Z,H_xstate,Ptrue);
    end
    tstate = [tstate t_px4flow];
    sensor_correction = true;
end 
    

return
