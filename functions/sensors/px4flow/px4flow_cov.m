% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   PX4 Optical Flow covariance matrix
%
%   [Vr,Vo,Vvxy] = px4flow_cov(of_s,params)
%
%   Sensor covariance may vary depending sensor range bounds.
%   
%   Inputs:
%       - of_s:      Sensor reading.
%       - params:    Sensor parameters.
% 
%   Outputs:
%       - std:      Current std to apply.
%       - Vr:       Range sensor covariance matrix.
%       - Vo:       Optical Flow sensor covariance matrix.
%       - Vvxy:     Vxy sensor covariance matrix.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [of_est_std,Vr,Vo,Vvxy] = px4flow_cov(of_s,params)

% Set reading and covariance depending on sensor bounds
if (of_s(1,1)<params.bounds.lower)
  of_est_std = [0.01;0.001;0.001;0.001;0.001]; % We assume robot landed.                   
elseif (of_s(1,1)>params.bounds.lower && of_s(1,1)<params.bounds.upper)
  of_est_std = params.std.insidebouds;
elseif (of_s(1,1)>params.bounds.upper) || isnan(of_s(1,1))
  of_est_std = params.std.outsidebouds;
end

Vr = diag(of_est_std(1,1).^2);
Vo = diag([of_est_std(1,1);of_est_std(2:3,1)].^2);
Vvxy = diag([of_est_std(1,1);of_est_std(4:5,1)].^2);

return