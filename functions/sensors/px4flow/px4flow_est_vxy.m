% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Optical Flow XY Velocity estimation when a NAN value is found in range measure
%
%   [of_s_new] = px4flow_est_vxy(of_s,state,w_s,met)
%
%   Returns the optical flow sensor measure with corrected flow 2D
%   
%   Inputs:
%       - of_s:     Original sensor reading with format [range;of_flow;of_vxy].
%       - state:    Current state vector.
%       - w_s:      Angular velocity of the sensor.
%       - met:      Method to apply: nothing, use last value or estimate
%
%   Ouputs:
%       - of_s_new: New sensor readings with corrected 2D flow
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [px4flow_s_new] = px4flow_est_vxy(px4flow_s,state,w_s,met)

%Camera Data
% CCD_width=4.51 %(mm)
% CCD_height=2.88 %(mm)
% f=16 %(mm)
% img_width=640 %(pix)
% img_height=480 %(pix)
% fw=img_width*f/CCD_width %pix
% fh=img_height*f/CCD_height %pix

%pix/event * event/0.01s * altitude(m)/focal(pix)
fw = 2.2705e+03; %pixel
fh = 2.6667e+03; %pixel

switch lower(met)
    case 'non'
        px4flow_s_new(:,1) = px4flow_s(:,end);
    case 'last'
        % Assign the last non nan value
        n=size(px4flow_s,2);
        px4flow_s_new(:,1) = px4flow_s(:,end);
        while (isnan(px4flow_s_new(1,1)) && n>1)
            px4flow_s_new(:,1) = px4flow_s(:,n);
            n=n-1;
        end
    case 'estimate'
        if isnan(px4flow_s(1,end))          
            % Using the state estimate as Z
            px4flow_s_new(1,1) = state(3);
            px4flow_s_new(2:3,1) = px4flow_s(2:3,end);
            px4flow_s_new(4,1) = 0.35*(-px4flow_s(2,end)/0.01)*px4flow_s_new(1,1)/fh + w_s(2,1)*px4flow_s_new(1,1); % Vx
            px4flow_s_new(5,1) = 0.35*(-px4flow_s(3,end)/0.01)*px4flow_s_new(1,1)/fw - w_s(1,1)*px4flow_s_new(1,1); % Vy       
        else
            px4flow_s_new(:,1) = px4flow_s(:,end);
        end   
        
end

return