% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Plots a 2D ellipsoid in a new figure
%
%   [] = drawEllipse2D(fig,data,conf,color)
%
%   - Inputs:
%       - fig:      Figure numer.
%       - data:   	Ellipsiod axis.
%       - conf:     Confidence interval error.
%       - color:    Color.
%
%   - Outputs:
%       - points:   Points of the circle.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [] = drawEllipse2D(fig,data,conf,color)

data = [data(1,:)' data(2,:)'];
    
% Calculate the eigenvectors and eigenvalues
covariance = cov(data);
[eigenvec, eigenval ] = eig(covariance);

% Get the index of the largest eigenvector
[largest_eigenvec_ind_c, r] = find(eigenval == max(max(eigenval)));
largest_eigenvec = eigenvec(:, largest_eigenvec_ind_c);

% Get the largest eigenvalue
largest_eigenval = max(max(eigenval));

% Get the smallest eigenvector and eigenvalue
if(largest_eigenvec_ind_c == 1)
    smallest_eigenval = max(eigenval(:,2));
    smallest_eigenvec = eigenvec(:,2);
else
    smallest_eigenval = max(eigenval(:,1));
    smallest_eigenvec = eigenvec(1,:);
end

% Calculate the angle between the x-axis and the largest eigenvector
angle = atan2(largest_eigenvec(2), largest_eigenvec(1));

% This angle is between -pi and pi.
% Let's shift it such that the angle is between 0 and 2pi
if(angle < 0)
    angle = angle + 2*pi;
end

% Get the coordinates of the data mean
avg = mean(data);

% Get the confidence interval error ellipse
switch conf
    case 1
        chisquare_val = sqrt(0.02);
    case 5
        chisquare_val = sqrt(0.10);
    case 10
        chisquare_val = sqrt(0.21);
    case 90
        chisquare_val = sqrt(4.61);
    case 95
        chisquare_val = sqrt(5.99);
    case 99
        chisquare_val = sqrt(9.21);
end
theta_grid = linspace(0,2*pi);
phi = angle;
X0=avg(1);
Y0=avg(2);
a=chisquare_val*sqrt(largest_eigenval);
b=chisquare_val*sqrt(smallest_eigenval);

% the ellipse in x and y coordinates 
ellipse_x_r  = a*cos( theta_grid );
ellipse_y_r  = b*sin( theta_grid );

%Define a rotation matrix
R = [ cos(phi) sin(phi); -sin(phi) cos(phi) ];

%let's rotate the ellipse to some angle phi
r_ellipse = [ellipse_x_r;ellipse_y_r]' * R;

% Draw the error ellipse
figure(fig),
hold on
plot(r_ellipse(:,1) + X0,r_ellipse(:,2) + Y0,'-','Color',color,'LineWidth',3);
hold on;

return