% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Get points of handler
%
%   [points]=get_points(hobj)
%
%   Returns the points conatined in the handler (for drawing pruposes)
%   
%   - Inputs:
%       - hobj:     Object handler.
% 
%   - Outputs:
%       - points:   Points in the handler.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [points]=get_points(hobj)

x0 = get(hobj,'XData')';
y0 = get(hobj,'YData')';
z0 = get(hobj,'ZData')';

x=[];
y=[];
z=[];

for ii=1:size(x0,1)
    x = [x x0(ii,:)];
end

for ii=1:size(y0,1)
    y = [y y0(ii,:)];
end
    
for ii=1:size(z0,1)
    z = [z z0(ii,:)];
end

points=[x;y;z];

return