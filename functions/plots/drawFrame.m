% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Generate the 3D frame (axis coordinates) handlers with rgb -> xyz.
%
%   [hx,posx,hy,posy,hz,posz] = drawFrame(parent,pos)
%
%   Inputs:
%       - parent:   Axes handler where the frame will be plotted. 
%       - pos:      Pose of the frame in 3D.
%       - long:     Axis length.
%
%   Ouputs:
%       - frame:    Frame handlers.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [frame] = drawFrame(parent,pos,length)

posx=[pos(1,1) pos(1,1)+length;
      pos(2,1) pos(2,1);
      pos(3,1) pos(3,1)];

hx=drawLine(parent,posx,'r');  
  
posy=[pos(1,1) pos(1,1);
      pos(2,1) pos(2,1)+length;
      pos(3,1) pos(3,1)];
      
hy=drawLine(parent,posy,'g');

posz=[pos(1,1) pos(1,1);
      pos(2,1) pos(2,1);
      pos(3,1) pos(3,1)+length];
hz=drawLine(parent,posz,'b');
  
frame=[hx,hy,hz];
hold on
return