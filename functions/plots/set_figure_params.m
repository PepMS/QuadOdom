% 
% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Creates a new figure with common propierties already set
%
%   [] = set_figure_params()
%
%   Set parameters to figure handle.
%   
%   Inputs:
%       - hfig:  Figure handler  
%       - title: Figure title
%       - fontsize: Font size
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [] = set_figure_params(hfig,title,fontsize,pos)

set(hfig,'NumberTitle','off','Name',title,'Renderer','Painters','defaulttextinterpreter','latex');
set(gca,'FontSize',fontsize,'box','off','layer','top','TickDir','out')
set(gcf,'Color',[1,1,1])
set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex')

if ~isempty(pos)
    set(hfig, 'Position', pos);
end

% set(gca,'LooseInset',get(gca,'TightInset'))

hold on;

return