% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Plot 3-vectors in 3 subplots (1 to 3 input vectors)
%
%   []=plot_3vec(fig,t,a_s,a,name,color,lab1,lab2)
%
%   Allows individual or various inputs:
%
%   1 3-vector: []=plot_3vec(fig,name,x,y1,lab1,c1)
%   2 3-vector: []=plot_3vec(fig,name,x,y1,lab1,c1,y2,lab2,c2)
%   3 3-vector: []=plot_3vec(fig,name,x,y1,lab1,c1,y2,lab2,c2,y3,lab3,c3)
%
%   Inputs:
%       - fig:      Figure number. 
%       - name:     Figure name.
%       - x:        X axis vector.
%       - y1:       First Y vector.
%       - lab1:     First Y vector legend label.
%       - c1:       First Y vector color.
%       - y2:       Second Y vector.
%       - lab2:     Second Y vector legend label.
%       - c2:       Second Y vector color.   
%       - y3:       Third Y vector.
%       - lab3:     Third Y vector legend label.
%       - c3:       Third Y vector color.   
%       - set_axis: Set all Y axis the same value.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function []=plot_3vec(fig,name,xaxislab,yaxislab1,yaxislab2,yaxislab3,x,y1,lab1,c1,y2,lab2,c2,y3,lab3,c3,set_axis)

hfig = figure(fig);
fontsize = 30;
hold on

if nargin == 10 % One 3-vector
    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); ylabel(yaxislab1); ylim([min(min(y1(1,:))) max(max(y1(1,:)))]);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); ylabel(yaxislab2); ylim([min(min(y1(2,:))) max(max(y1(2,:)))]);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); ylabel(yaxislab3); ylim([min(min(y1(3,:))) max(max(y1(3,:)))]);
    xlabel(xaxislab);    
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1);

elseif nargin == 11 % One 3-vector + all axis with the same values
    set_axis=y2;
    sy=[min(min(y1)) max(max(y1))];
    sx=[min(x) max(x)];
    axs=[sx sy];
    
    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); if set_axis==1 axis(axs); end, ylabel(yaxislab1);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); if set_axis==1 axis(axs); end, ylabel(yaxislab2);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); if set_axis==1 axis(axs); end, ylabel(yaxislab3);
    xlabel(xaxislab); 
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1);

elseif nargin == 13 % Two 3-vector
    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); plot(x,y2(1,:),c2); ylabel(yaxislab1);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); plot(x,y2(2,:),c2); ylabel(yaxislab2);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); plot(x,y2(3,:),c2); ylabel(yaxislab3);
    xlabel(xaxislab);
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1,lab2);

elseif nargin == 14 % Two 3-vector + all axis with the same values
    set_axis=y3;
    ymax=max(max(max(y1)),max(max(y2)));
    ymin=min(min(min(y1)),min(min(y2)));
    sy=[ymin ymax];
    sx=[min(x) max(x)];
    axs=[sx sy];

    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); plot(x,y2(1,:),c2);if set_axis==1 axis(axs); end, ylabel(yaxislab1);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); plot(x,y2(2,:),c2);if set_axis==1 axis(axs); end, ylabel(yaxislab2);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); plot(x,y2(3,:),c2);if set_axis==1 axis(axs); end, ylabel(yaxislab3);
    xlabel(xaxislab);
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1,lab2);

elseif nargin == 16 % Three 3-vector
    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); plot(x,y2(1,:),c2); plot(x,y3(1,:),c3); ylabel(yaxislab1);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); plot(x,y2(2,:),c2); plot(x,y3(2,:),c3); ylabel(yaxislab2);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); plot(x,y2(3,:),c2); plot(x,y3(3,:),c3); ylabel(yaxislab3);
    xlabel(xaxislab);
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1,lab2,lab3);
    
elseif nargin == 17  % Three 3-vector + all axis with the same values
    ymax=max(max(max(y1)),max(max(y2)));
    ymax=max(ymax,max(max(y3)));
    ymin=min(min(min(y1)),min(min(y2)));
    ymin=min(ymin,min(min(y3)));
    sy=[ymin ymax];
    sx=[min(x) max(x)];
    axs=[sx sy];

    h11=subplot(3,1,1);hold on; plot(x,y1(1,:),c1); plot(x,y2(1,:),c2); plot(x,y3(1,:),c3);if set_axis==1 axis(axs); end, ylabel(yaxislab1);
    h12=subplot(3,1,2);hold on; plot(x,y1(2,:),c1); plot(x,y2(2,:),c2); plot(x,y3(2,:),c3);if set_axis==1 axis(axs); end, ylabel(yaxislab2);
    h13=subplot(3,1,3);hold on; plot(x,y1(3,:),c1); plot(x,y2(3,:),c2); plot(x,y3(3,:),c3);if set_axis==1 axis(axs); end, ylabel(yaxislab3);
    xlabel(xaxislab);
    linkaxes([h11 h12 h13],'x');
    h=legend([h11],lab1,lab2,lab3);
else
    error('Not enough input parameters.');
end

set(h,'interpreter','latex','fontsize',fontsize);

set_figure_params(hfig,name,fontsize,[100 100 1050 640]);

hold off

return