 #!/bin/bash  

args=("$@")  

if [ "$1" == "-h" ]; then

  echo USAGE: - Input parameters: BAG_FILE DEST_PATH. Example of usage: ./genfiles.sh ../../data/real/rosbags/test.bag "~/git/asantamaria_matlab/QuadOdom/data/real/rosbags"

elif [ $# != 2 ] ; then


  echo ERROR: Wrong input. Run -h to see options.

  exit 0

else
  
  echo ...processing ground truth odometry...

  # Ground truth pose
  rostopic echo -b $1 -p /QuadrotorOscar/odom > $2/gtruth_pose.txt
  sed -i 's/\/vicon//g' $2/gtruth_pose.txt
  sed -i 's/QuadrotorOscar\/base_link//g' $2/gtruth_pose.txt



  echo ...processing imu...

  # IMU topic
  rostopic echo -b $1 -p /quad_decode_msg/imu/header > $2/imu_header.txt
  sed -i 's/\/quadrotor//g' $2/imu_header.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/linear_acceleration > $2/linear_acceleration.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/linear_acceleration_covariance > $2/linear_acceleration_covariance.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/angular_velocity > $2/angular_velocity.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/angular_velocity_covariance > $2/angular_velocity_covariance.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/orientation > $2/orientation.txt
  rostopic echo -b $1 -p /quad_decode_msg/imu/orientation_covariance > $2/orientation_covariance.txt

  echo ...processing px4flow...
  
  # PX4Flow
  rostopic echo -b $1 -p /px4flow/opt_flow > $2/opt_flow.txt
  sed -i 's/\/px4flow//g' $2/opt_flow.txt
  
  echo ...processing range...

  # Range
  rostopic echo -b $1 -p /teraranger/timeofflight > $2/range.txt
  sed -i 's/base_link//g' $2/range.txt
  
  echo Done.
  
fi
