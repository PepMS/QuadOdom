% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   IMU from ROSBAG
% 
%   [tstamp,a_s,w_s,q_s,A,W,Q] = imu_from_rosbag(rosbag_file,topic_name)
% 
%   This script retrieves Accelerometer, Gyroscope and Orientation messages from a
%   robsbag containing an IMU topic.
% 
%   - Inputs:
%       - bagfile:      Rosbag file (see 'help rosbag').
%       - topic_name:   Imu_topic name.
% 
%   - Outputs:
%       - tstamp:       Messages time stamp. Initialized w.r.t. bag time.
%       - a_s:          Accelerometer readings. [ax ay az]
%       - w_s:          Gyroscope readings. [wx wy wz]
%       - q_s:          Orientation quaternion. Hamilton [qw qx qy qz].
%       - A:            Accelerometer Covariances. [3x3]
%       - W:            Gyroscope Covariances. [3x3]
%       - Q:            Orientation Covariances. [3x3]
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [tstamp,a_s,w_s,q_s,A,W,Q] = imu_from_rosbag(bagfile,topic_name)

disp('-> Loading IMU data...')

% Get topic
Data = select(bagfile,'Topic',topic_name);

% Get messages
Msgs = readMessages(Data);

% Initialize structures
numMsgs = size(Msgs,1);
tstamp = zeros(1,numMsgs);
a_s = zeros(3,numMsgs);
w_s = zeros(3,numMsgs);
q_s = zeros(4,numMsgs);
A = zeros(3,3,numMsgs);
W = zeros(3,3,numMsgs);
Q = zeros(3,3,numMsgs);

% get initial BAG time
tini = double(bagfile.StartTime);

% Mount output structures
for ii = 1:numMsgs

   tstamp(1,ii) = double(Msgs{ii}.Header.Stamp.Sec) ...
                  + double(Msgs{ii}.Header.Stamp.Nsec)*1e-9 ...
                  - tini;
              
   a_s(:,ii) = [double(Msgs{ii}.LinearAcceleration.X); ...
                double(Msgs{ii}.LinearAcceleration.Y); ...
                double(Msgs{ii}.LinearAcceleration.Z)];
      
   w_s(:,ii) = [double(Msgs{ii}.AngularVelocity.X); ...
                double(Msgs{ii}.AngularVelocity.Y); ...
                double(Msgs{ii}.AngularVelocity.Z)];
        
   q_s(:,ii) = [double(Msgs{ii}.Orientation.W); ...
                double(Msgs{ii}.Orientation.X); ...
                double(Msgs{ii}.Orientation.Y); ...
                double(Msgs{ii}.Orientation.Z)];
               
   A(:,:,ii) = reshape(double(Msgs{ii}.LinearAccelerationCovariance),[3,3]);  
   
   W(:,:,ii) = reshape(double(Msgs{ii}.AngularVelocityCovariance),[3,3]);  
   
   Q(:,:,ii) = reshape(double(Msgs{ii}.OrientationCovariance),[3,3]);  
    
end

return
