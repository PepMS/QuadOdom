% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   ROS to MATLAB quaternion
% 
%   [matq] = rosq2matq(rosq)
% 
%   This functions transforms from ROS to MATLAB quaternion formats.
% 
%   - Inputs:
%       - rosq:     ROS quaternion [qx;qy;qz;qw]
% 
%   - Outputs:
%       - matq:     MATLAB quaternion [qw;qx;qy;qz]
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [matq] = rosq2matq(rosq)

matq = zeros(size(rosq));

if size(rosq,1) == 4
    matq(1,:) = rosq(4,:);
    matq(2:4,:) = rosq(1:3,:);    
elseif size(rosq,2) == 4
    matq(:,1) = rosq(:,4);
    matq(:,2:4) = rosq(:,1:3);    
end

return