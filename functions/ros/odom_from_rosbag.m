% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   Odometry from ROSBAG
% 
%   [tstamp,odom] = odom_from_rosbag(bagfile,topic_name)
% 
%   This script retrieves Odometry messages from a
%   robsbag containing an nav_msgs/Odometry topic.
% 
%   - Inputs:
%       - bagfile:      Rosbag file (see 'help rosbag').
%       - topic_name:   Imu_topic name.
% 
%   - Outputs:
%       - tstamp:       Messages time stamp. Initialized w.r.t. bag time.
%       - odom:         Odometry readings. [p v q w]'. Quaternion uses
%                       Hamilton conv. [qw qx qy qz].
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [tstamp,odom] = odom_from_rosbag(bagfile,topic_name)

disp('-> Loading GROUND TRUTH odom. data...')

% Get topic
Data = select(bagfile,'Topic',topic_name);

% Get messages
Msgs = readMessages(Data);

% Initialize structures
numMsgs = size(Msgs,1);
tstamp = zeros(1,numMsgs);
odom = zeros(13,numMsgs);

% get initial BAG time
tini = double(bagfile.StartTime);

% Mount output structures
for ii = 1:numMsgs

   tstamp(1,ii) = double(Msgs{ii}.Header.Stamp.Sec) ...
                  + double(Msgs{ii}.Header.Stamp.Nsec)*1e-9 ...
                  - tini;
                           
   odom(:,ii) = [double(Msgs{ii}.Pose.Pose.Position.X); ...
                 double(Msgs{ii}.Pose.Pose.Position.Y); ...
                 double(Msgs{ii}.Pose.Pose.Position.Z); ...
                 double(Msgs{ii}.Twist.Twist.Linear.X); ...
                 double(Msgs{ii}.Twist.Twist.Linear.Y); ...
                 double(Msgs{ii}.Twist.Twist.Linear.Z); ...
                 double(Msgs{ii}.Pose.Pose.Orientation.W); ...
                 double(Msgs{ii}.Pose.Pose.Orientation.X); ...
                 double(Msgs{ii}.Pose.Pose.Orientation.Y); ...
                 double(Msgs{ii}.Pose.Pose.Orientation.Z); ...                 
                 double(Msgs{ii}.Twist.Twist.Angular.X); ...
                 double(Msgs{ii}.Twist.Twist.Angular.Y); ...
                 double(Msgs{ii}.Twist.Twist.Angular.Z)];
  
end

return