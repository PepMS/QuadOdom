% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
%
%   CREATE FILTER NAME
% 
%   [name] = create_filter_name(filter_opts)
% 
%   Script to create a name (string) for each filter type, depending on the
%   options.
% 
%   - Inputs:
%       - filter_opts:  Filter options (see initialization files for details).
% 
%   - Outputs:
%       - name:         Filter name.
% 
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [name] = create_filter_name(filter_opts)

% Quaternion method name
if strcmp(filter_opts.qint_met,'zerof')
    qmet = '0F';
elseif strcmp(filter_opts.qint_met,'zerob')
    qmet = '0B';
elseif strcmp(filter_opts.qint_met,'first')
    qmet = '1ST';
end
            
% Frame method name
if strcmp(filter_opts.frame,'local')
    fmet = 'L';
elseif strcmp(filter_opts.frame,'global')
    fmet = 'G';
end
            
name = sprintf('%s_{%s+N%d+Q%s}.mat', upper(filter_opts.met), fmet,filter_opts.imu_met,qmet);

return