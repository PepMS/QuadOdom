% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MATLAB QuadOdom. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with QuadOdom.  If not, see <http://www.gnu.org/licenses/>.
% 
%   __________________________________________________________________
% 
%   Print FILTER options.
%
%   []=todo(opt)
%
%   Inputs:
%       - opt:  Struct with the filter options (experiment).
%       - path: Data path.
%
%   Copyright 2016 asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [] = print_options(opt,path,nrun)

color = 2;

fprintf('-> RUNNING FILTER: ');
fprintf(['<strong>' opt.met '</strong>']);
fprintf('  FRAME: ');
fprintf(['<strong>' opt.frame '</strong>']);
fprintf('  IMU trunc: ');
fprintf(['<strong>' int2str(opt.imu_met) '</strong>']);
fprintf('  qInt: ');
fprintf(['<strong>' opt.qint_met '</strong>']);
fprintf('  OBS. RATE: ');
fprintf(['<strong>' int2str(opt.obs_rate) '</strong>']);
fprintf('  DATA PATH: ');
fprintf(['<strong>' path '</strong>']);
fprintf('  RUN: ');
fprintf(['<strong>' int2str(nrun) '</strong>']);
fprintf('\n');

return